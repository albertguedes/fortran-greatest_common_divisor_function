!       main.f95 - A simples program to calculate the greatest common divisor between two integers.
!       
!       created : 05/01/2015
!       
!       author: albert r. carnier guedes ( albert@teko.net.br ) 
!       
!       
	PROGRAM GREATEST_COMMON_DIVISOR
	
	integer p,q

!       Read two integers from cli.
	read(*,*) p,q
	
!       Print the gdc of 'p' and 'q'.
	print*, int( GCD(p,q) )
	
	END PROGRAM GREATEST_COMMON_DIVISOR
	
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	FUNCTIONS AND SUBROUTINES       !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
!       
!       Function that calculate GCD between two integers. 
!       
	real FUNCTION GCD(p,q) RESULT(z)
	
	integer p,q
	integer r,x,y
	
	x=p
	y=q
	
!       Verify if 'p' is lower than 'q'
	if(p.lt.q) then
	   x=q
	   y=p
	end if
	
!       Iteratively calculates the remainder of the division of 'x' by 'y'.
	do while( y.ne.0 )
	   r=modulo(x,y)
	   x=y
	   y=r
	enddo

!       The last result of 'x' is the gcd.
	z=x
	
	return
	
	END FUNCTION GCD
